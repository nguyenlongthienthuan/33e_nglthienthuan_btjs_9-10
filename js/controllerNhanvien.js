function nv(taikhoannv,hovatenNV,emailNV,matkhauNV,ngaylamNV,chucvuNV,giolamNV,luongCBNV,tongluong,xeploai){
  this.tk=taikhoannv;
  this.name=hovatenNV;
  this.mail=emailNV;
  this.pass=matkhauNV;
  this.datepicker=ngaylamNV;
  this.chucvu=chucvuNV;
  this.time=giolamNV;
  this.luong=luongCBNV;
  this.tongluong=tongluong;
  this.xeploai=xeploai;
}
function laythongtin_NV(){
    
    var taikhoannv=document.getElementById("tknv").value;
    var hovatenNV=document.getElementById("name").value;
    var emailNV=document.getElementById("email").value;
    var matkhauNV=document.getElementById("password").value;
    var ngaylamNV=document.getElementById("datepicker").value;
    var chucvuNV=document.getElementById("chucvu").value;
    var luongCBNV=document.getElementById("luongCB").value*1;
    var giolamNV=document.getElementById("gioLam").value*1;
    function tongluong (){
        if (chucvuNV=="Sếp"){
            return (luongCBNV*3);
        }else if(chucvuNV=="Trưởng phòng")
        {
            return (luongCBNV*2);
        }else{
            return (luongCBNV*1);
        }
    }
    function xeploai(){
        if (giolamNV>=192){
            return "xuất sắc";
        }
        else if(giolamNV>=176){
            return "giỏi";
        }
        else if(giolamNV>=160){
            return "khá";
        }else{
            return "trung bình";
        }
    }
    var nhanvien=new nv(taikhoannv,hovatenNV,emailNV,matkhauNV,ngaylamNV,chucvuNV,giolamNV,luongCBNV,tongluong(),xeploai());
    return nhanvien;
}

function checkthongtinnhap(arr,nhanvien,index){
    if(index==0) { 
     {
        for(var i=0;i<arr.length;i++){
            if(nhanvien.tk==arr[i].tk){
              thongbao("tbTKNV","tk này đã tồn tại,vui lòng nhập tk khác");
              return 2;
            };
          }
          if(nhanvien.tk=="" || isNaN(nhanvien.tk) || nhanvien.tk.length>6 || nhanvien.tk.length<4){
            thongbao("tbTKNV","tk gồm 4-6 kí số và không dc để trống")
            return false;
        } else{
           reset_TB("tbTKNV");
          return true;
        }
      }
    }
    else if(index==1)
      {   var regexname=/^[a-zA-Z]+$/;
        var checkstr=nhanvien.name.split(" ").join("");
        if(nhanvien.name==""){
          thongbao("tbTen","không dc để trống ô này");
            return false;
        }else if (nhanvien.name!="")
        {       
          if (checkstr.match(regexname)==null){
            thongbao("tbTen","tên chỉ bao gồm chữ cái");
                  return false;
          }else {reset_TB("tbTen"); return true}
        } 
      }
      else if(index==2){
        var dinhdangmail=/^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
        if (nhanvien.mail.match(dinhdangmail)==null)
        {   
           thongbao("tbEmail","Email phải đúng định dạng, không để trống");
          return false;
        }
         else{
          reset_TB("tbEmail");
          return true;
         }
      }
      else if(index==3){
        var strongRegex = new RegExp("^(?=.{6,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    if (nhanvien.pass.match(strongRegex)==null)
    {   
      thongbao("tbMatKhau","mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống ")
      return false;
    }
     else{
      reset_TB("tbMatKhau");
      return true;
     }
      }
      else if (index==4){
        var regex=/^(((0[13-9]|1[012])[-/]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?(0[1-9]|1[0-9]|2[0-8]))[-/]?[0-9]{4}|02[-/]?29[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;
         if(nhanvien.datepicker!=""){
           if(regex.test(nhanvien.datepicker)){
            reset_TB("tbNgay");
             return true;
           }else {
              thongbao("tbNgay","Ngày làm không để trống, định dạng mm/dd/yyyy")
              return false;
           }
         }else {
          thongbao("tbNgay","Ngày làm không để trống")
              return false;
         }
      }
      else if(index==7){
        if(nhanvien.chucvu=="Chọn chức vụ") 
        {
           thongbao("tbChucVu","Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)")
          return false;
        }
       else{
        reset_TB("tbChucVu");
        return true;
       }
      }
      else if(index==6){
        if (nhanvien.time==""){

          thongbao("tbGiolam","không dc để trống ô này");
          return false;
        }
        else {
            if(nhanvien.time<80 || nhanvien.time>200)
           {
             thongbao("tbGiolam","Số giờ làm trong tháng 80 - 200 giờ, không để trống")
            return false;
           }
            {
               reset_TB("tbGiolam");
              return true;
            }
        }
      } 
      else if(index==5){
        if (nhanvien.luong==""){
          thongbao("tbLuongCB","không dc để trống ô này");
          return false
        }
        else {
            if(nhanvien.luong<1000000 || nhanvien.luong>20000000)
            {
               thongbao("tbLuongCB","Lương cơ bản 1 000 000 - 20 000 000, không để trống");
              return false;
            }
            else {
              reset_TB("tbLuongCB");
              return true;
            }
        }
      }
    }

// thong bao the span
function thongbao(id,text){
  document.getElementById(id).style.display="block";
  document.getElementById(id).innerHTML=text;
}
function reset_TB(id){
document.getElementById(id).style.display="none";
}
// thong bao nhap sai khi nhap o input
function thongbao_input(option){
  document.getElementById("tknv").disabled = false;
    document.querySelectorAll(".modal-body input").forEach(function(input,index_input){
     input.addEventListener("keyup",function(){
    if(option=="update" ){
      if( index_input!=3){checkthongtinnhap(nhanvien_arr,laythongtin_NV(),index_input);  }
      else{ reset_TB("tbMatKhau") } // update thi ko can check mk;
    }else if(option=="add"){
      checkthongtinnhap(nhanvien_arr,laythongtin_NV(),index_input);
    }
     }) 
            })
            document.getElementById("chucvu").addEventListener("click",function check(){
              checkthongtinnhap(nhanvien_arr,laythongtin_NV(),7);
            })
}
// function in danh sach nv
function render_NV(arr){
    
   var text="";
    for (var i=0;i<arr.length;i++){
       if(arr[i].tk==""){
        arr.splice(i,1);
        i--;
       }else{
        var textHTML=`
        <tr>
          <td>${arr[i].tk}</td>
          <td>${arr[i].name}</td>
          <td>${arr[i].mail}</td>
          <td>${arr[i].datepicker}</td>
          <td>${arr[i].chucvu}</td>
          <td>${arr[i].tongluong}</td>
          <td>${arr[i].xeploai}</td>
          <td><button class="delete">delete</button><button class="edit" >edit</button></td>
        </tr>
        `
        text+=textHTML;
       }
    }
   document.getElementById("tableDanhSach").innerHTML=text;
   button_deleteNV(arr);
   button_edit();
    }
// function delete
function deleteNV(arr,index_del){
    var check_tk= document.getElementById("tableDanhSach").querySelectorAll("tr")[index_del].querySelectorAll("td")[0].innerText;
       for (var i=0;i<arr.length;i++){
         if(arr[i].tk==check_tk){
             arr[i].tk="";
             break;
         }
       }save_Storage(arr);
     document.getElementById("tableDanhSach").querySelectorAll("tr")[index_del].style.display="none";
    //  document.getElementById("tableDanhSach").querySelectorAll("tr")[index_del].querySelectorAll("td")[0].innerHTML="";
 }
//  function edit hiện bảng edit thông tin để cập nhật nv
function edit_NV(index_edit){
         document.querySelector("body").classList.add("modal-open");
           document.querySelector("body").style.paddingRight="17px";
           document.querySelector("#myModal").classList.add("show");
           document.querySelector("#myModal").style.display="block";
           document.querySelector("#myModal").style.paddingRight="17px";
           thongbao_input("update");
           document.getElementById("tknv").disabled = true;
           document.querySelectorAll(".sp-thongbao").forEach(function(sp){sp.style.display="none";})    
          //  document.getElementById("btnCapNhat").addEventListener("click",function (){
          //   capnhat_NV(nhanvien_arr,laythongtin_NV());
          //  })
           document.querySelector("#btnDong").addEventListener("click" ,
              function reset(){ // button đóng tab nhập thông tin
            document.querySelector("body").classList.remove("modal-open");
            document.querySelector("body").style.paddingRight="";
            document.querySelector("#myModal").classList.remove("show");
            document.querySelector("#myModal").style.display="none";
            document.querySelector("#myModal").style.paddingRight="";
           })   
          var checkid= document.getElementById("tknv").value=document.getElementById("tableDanhSach").querySelectorAll("tr")[index_edit].querySelectorAll("td")[0].innerText;
           document.getElementById("name").value=nhanvien_arr[index_edit].name;
           document.getElementById("email").value=nhanvien_arr[index_edit].mail;
           document.getElementById("password").value="";
           document.getElementById("datepicker").value=nhanvien_arr[index_edit].datepicker;
           document.getElementById("chucvu").value=nhanvien_arr[index_edit].chucvu;
           document.getElementById("luongCB").value=nhanvien_arr[index_edit].luong;
           document.getElementById("gioLam").value=nhanvien_arr[index_edit].time;    
}
// function cap nhat nv
function capnhat_NV(arr,nhanvien){
  if(check_thongtinhople(arr,nhanvien,"update") ){
    
    if(nhanvien.pass!=""){
      reset_TB("tbMatKhau");
      for (var i=0;i<nhanvien_arr.length;i++){
        if(arr[i].tk==nhanvien.tk ){
           if( nhanvien.pass==arr[i].pass){ document.getElementById("tbMatKhau").innerHTML="none";
           arr[i]=nhanvien;
         document.getElementById("tableDanhSach").querySelectorAll("tr")[i].innerHTML=`
         <td>${arr[i].tk}</td>
         <td>${arr[i].name}</td>
         <td>${arr[i].mail}</td>
         <td>${arr[i].datepicker}</td>
         <td>${arr[i].chucvu}</td>
         <td>${arr[i].tongluong}</td>
         <td>${arr[i].xeploai}</td>
         <td><button class="delete">delete</button><button class="edit" >edit</button></td>
         `
          button_edit();
          button_deleteNV(arr);
           save_Storage(arr);
           break;
          }
          else{ document.getElementById("tbMatKhau").innerHTML="mk sai";
          document.getElementById("tbMatKhau").style.display="block";}
         break;
        }}} else{thongbao("tbMatKhau","hãy nhập đúng mk để update thông tin")}}}
  // function tim nv theo loai
function duyetmangnhanvien(arr,obj,value){ 
  for (var i=0;i<arr.length;i++){
   if(value!="" ){ 
     if((arr[i][obj]).indexOf(value)!==-1 && arr[i]["tk"]!="")
    {     
      document.getElementById("tableDanhSach").querySelectorAll("tr")[i].style.display="table-row";
    }
    else {
      document.getElementById("tableDanhSach").querySelectorAll("tr")[i].style.display="none";
  }
   }else{
   if (arr[i]["tk"]!=""){
    document.getElementById("tableDanhSach").querySelectorAll("tr")[i].style.display="table-row";
   }}}}


